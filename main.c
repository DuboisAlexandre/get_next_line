/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 12:43:04 by adubois           #+#    #+#             */
/*   Updated: 2015/12/11 11:50:07 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "get_next_line.h"

typedef struct	s_check
{
	int		fd;
	char	*file;
	char	*cmd;
	char	*result;
}				t_check;

void	std_in_checks(void)
{
	char	*line;

	while (get_next_line(0, &line) == 1)
	{
		write(1, line, strlen(line));
		write(1, "\n", 1);
	}
}

void	simple_checks(void)
{
	int		i;
	int		nb;
	int		fd;
	t_check	checks[13];
	char	*line;
	char	*cmp;
	char	*result;

	cmp = (char *)malloc(100);

	checks[0].file = (char *)malloc(50);
	checks[0].cmd = (char *)malloc(100);
	checks[0].file = strcpy(checks[0].file, "sandbox/empty");
	checks[0].cmd = strcpy(checks[0].cmd, "echo \"\" | tr -d '\n' | tr -d '\r' > ");
	strcat(checks[0].cmd, checks[0].file);
	system(checks[0].cmd);
	checks[0].fd = open(checks[0].file, O_RDONLY);
	checks[0].result = strdup("");
	
	checks[1].file = (char *)malloc(50);
	checks[1].cmd = (char *)malloc(100);
	checks[1].file = strcpy(checks[1].file, "sandbox/new_line");
	checks[1].cmd = strcpy(checks[1].cmd, "echo \"\" > ");
	strcat(checks[1].cmd, checks[1].file);
	system(checks[1].cmd);
	checks[1].fd = open(checks[1].file, O_RDONLY);
	checks[1].result = strdup("\n");
	
	checks[2].file = (char *)malloc(50);
	checks[2].cmd = (char *)malloc(100);
	checks[2].file = strcpy(checks[2].file, "sandbox/3_new_line");
	checks[2].cmd = strcpy(checks[2].cmd, "echo \"\n\n\" > ");
	strcat(checks[2].cmd, checks[2].file);
	system(checks[2].cmd);
	checks[2].fd = open(checks[2].file, O_RDONLY);
	checks[2].result = strdup("\n\n\n");
	
	checks[3].file = (char *)malloc(50);
	checks[3].cmd = (char *)malloc(100);
	checks[3].file = strcpy(checks[3].file, "sandbox/single_line");
	checks[3].cmd = strcpy(checks[3].cmd, "echo \"This is a single line file\" | tr -d '\n' | tr -d '\r' > ");
	strcat(checks[3].cmd, checks[3].file);
	system(checks[3].cmd);
	checks[3].fd = open(checks[3].file, O_RDONLY);
	checks[3].result = strdup("This is a single line file\n");
	
	checks[4].file = (char *)malloc(50);
	checks[4].cmd = (char *)malloc(100);
	checks[4].file = strcpy(checks[4].file, "sandbox/simple");
	checks[4].cmd = strcpy(checks[4].cmd, "echo \"This is\na simple file\" > ");
	strcat(checks[4].cmd, checks[4].file);
	system(checks[4].cmd);
	checks[4].fd = open(checks[4].file, O_RDONLY);
	checks[4].result = strdup("This is\na simple file\n");
	
	checks[5].file = (char *)malloc(50);
	checks[5].cmd = (char *)malloc(100);
	checks[5].file = strcpy(checks[5].file, "sandbox/multiline");
	checks[5].cmd = strcpy(checks[5].cmd, "echo \"This is\na multiline\nfile\n\" > ");
	strcat(checks[5].cmd, checks[5].file);
	system(checks[5].cmd);
	checks[5].fd = open(checks[5].file, O_RDONLY);
	checks[5].result = strdup("This is\na multiline\nfile\n\n");
	
	checks[6].file = (char *)malloc(50);
	checks[6].cmd = (char *)malloc(100);
	checks[6].file = strcpy(checks[6].file, "sandbox/4_chars");
	checks[6].cmd = strcpy(checks[6].cmd, "echo \"0123\" | tr -d '\n' | tr -d '\r' > ");
	strcat(checks[6].cmd, checks[6].file);
	system(checks[6].cmd);
	checks[6].fd = open(checks[6].file, O_RDONLY);
	checks[6].result = strdup("0123\n");
	
	checks[7].file = (char *)malloc(50);
	checks[7].cmd = (char *)malloc(100);
	checks[7].file = strcpy(checks[7].file, "sandbox/4_chars_nl");
	checks[7].cmd = strcpy(checks[7].cmd, "echo \"0123\" > ");
	strcat(checks[7].cmd, checks[7].file);
	system(checks[7].cmd);
	checks[7].fd = open(checks[7].file, O_RDONLY);
	checks[7].result = strdup("0123\n");
	
	checks[8].file = (char *)malloc(50);
	checks[8].cmd = (char *)malloc(100);
	checks[8].file = strcpy(checks[8].file, "sandbox/8_chars");
	checks[8].cmd = strcpy(checks[8].cmd, "echo \"01234567\" | tr -d '\n' | tr -d '\r' > ");
	strcat(checks[8].cmd, checks[8].file);
	system(checks[8].cmd);
	checks[8].fd = open(checks[8].file, O_RDONLY);
	checks[8].result = strdup("01234567\n");
	
	checks[9].file = (char *)malloc(50);
	checks[9].cmd = (char *)malloc(100);
	checks[9].file = strcpy(checks[9].file, "sandbox/8_chars_nl");
	checks[9].cmd = strcpy(checks[9].cmd, "echo \"01234567\" > ");
	strcat(checks[9].cmd, checks[9].file);
	system(checks[9].cmd);
	checks[9].fd = open(checks[9].file, O_RDONLY);
	checks[9].result = strdup("01234567\n");
	
	checks[10].file = (char *)malloc(50);
	checks[10].cmd = (char *)malloc(100);
	checks[10].file = strcpy(checks[10].file, "sandbox/16_chars");
	checks[10].cmd = strcpy(checks[10].cmd, "echo \"0123456789ABCDEF\" | tr -d '\n' | tr -d '\r' > ");
	strcat(checks[10].cmd, checks[10].file);
	system(checks[10].cmd);
	checks[10].fd = open(checks[10].file, O_RDONLY);
	checks[10].result = strdup("0123456789ABCDEF\n");
	
	checks[11].file = (char *)malloc(50);
	checks[11].cmd = (char *)malloc(100);
	checks[11].file = strcpy(checks[11].file, "sandbox/16_chars_nl");
	checks[11].cmd = strcpy(checks[11].cmd, "echo \"0123456789ABCDEF\" > ");
	strcat(checks[11].cmd, checks[11].file);
	system(checks[11].cmd);
	checks[11].fd = open(checks[11].file, O_RDONLY);
	checks[11].result = strdup("0123456789ABCDEF\n");
	
	checks[12].fd = -1;

	i = 0;
	while (checks[i].fd >= 0)
	{
		result = (char *)malloc(50);
		strcat(checks[i].file, ".res");
		fd = open(checks[i].file, O_CREAT | O_RDWR | O_TRUNC, 0755);
		while (get_next_line(checks[i].fd, &line) == 1)
		{
			write(fd, line, strlen(line));
			write(fd, "\n", 1);
		}
		close(checks[i].fd);
		close(fd);
		fd = open(checks[i].file, O_RDONLY);
		nb = read(fd, cmp, 100);
		cmp[nb] = '\0';
		printf("Simple %d: %s\n", i, (strcmp(cmp, checks[i].result) == 0) ? "SUCCESS" : "FAILURE");
		close(fd);
		free(result);
		i++;
	}
}

void	big_checks(void)
{
	int		i;
	int		nb;
	int		fd;
	t_check	checks[8];
	char	*line;
	char	*cmp;
	char	*result;

	cmp = (char *)malloc(100);

	checks[0].file = (char *)malloc(50);
	checks[0].cmd = (char *)malloc(100);
	checks[0].file = strcpy(checks[0].file, "sandbox/256o");
	checks[0].cmd = strcpy(checks[0].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 256 > ");
	strcat(checks[0].cmd, checks[0].file);
	system(checks[0].cmd);
	checks[0].fd = open(checks[0].file, O_RDONLY);

	checks[1].file = (char *)malloc(50);
	checks[1].cmd = (char *)malloc(100);
	checks[1].file = strcpy(checks[1].file, "sandbox/512o");
	checks[1].cmd = strcpy(checks[1].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 512 > ");
	strcat(checks[1].cmd, checks[1].file);
	system(checks[1].cmd);
	checks[1].fd = open(checks[1].file, O_RDONLY);

	checks[2].file = (char *)malloc(50);
	checks[2].cmd = (char *)malloc(100);
	checks[2].file = strcpy(checks[2].file, "sandbox/1Ko");
	checks[2].cmd = strcpy(checks[2].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 1024 > ");
	strcat(checks[2].cmd, checks[2].file);
	system(checks[2].cmd);
	checks[2].fd = open(checks[2].file, O_RDONLY);

	checks[3].file = (char *)malloc(50);
	checks[3].cmd = (char *)malloc(100);
	checks[3].file = strcpy(checks[3].file, "sandbox/2Ko");
	checks[3].cmd = strcpy(checks[3].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 2048 > ");
	strcat(checks[3].cmd, checks[3].file);
	system(checks[3].cmd);
	checks[3].fd = open(checks[3].file, O_RDONLY);

	checks[4].file = (char *)malloc(50);
	checks[4].cmd = (char *)malloc(100);
	checks[4].file = strcpy(checks[4].file, "sandbox/5Ko");
	checks[4].cmd = strcpy(checks[4].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 5120 > ");
	strcat(checks[4].cmd, checks[4].file);
	system(checks[4].cmd);
	checks[4].fd = open(checks[4].file, O_RDONLY);

	checks[5].file = (char *)malloc(50);
	checks[5].cmd = (char *)malloc(100);
	checks[5].file = strcpy(checks[5].file, "sandbox/50Ko");
	checks[5].cmd = strcpy(checks[5].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 51200 > ");
	strcat(checks[5].cmd, checks[5].file);
	system(checks[5].cmd);
	checks[5].fd = open(checks[5].file, O_RDONLY);

	checks[6].file = (char *)malloc(50);
	checks[6].cmd = (char *)malloc(100);
	checks[6].file = strcpy(checks[6].file, "sandbox/100Ko");
	checks[6].cmd = strcpy(checks[6].cmd, "base64 /dev/urandom | tr -d '\n' | tr -d '\r' | head -c 102400 > ");
	strcat(checks[6].cmd, checks[6].file);
	system(checks[6].cmd);
	checks[6].fd = open(checks[6].file, O_RDONLY);

	checks[7].fd = -1;

	i = 0;
	while (checks[i].fd >= 0)
	{
		result = (char *)malloc(50);
		strcpy(result, checks[i].file);
		strcat(result, ".res");
		fd = open(result, O_CREAT | O_RDWR | O_TRUNC, 0755);
		while (get_next_line(checks[i].fd, &line) == 1)
		{
			write(fd, line, strlen(line));
			write(fd, "\n", 1);
		}
		close(checks[i].fd);
		close(fd);
		strcpy(checks[i].cmd, "echo \"\" >> ");
		strcat(checks[i].cmd, checks[i].file);
		system(checks[i].cmd);
		strcpy(checks[i].cmd, "diff ");
		strcat(checks[i].cmd, checks[i].file);
		strcat(checks[i].cmd, " ");
		strcat(checks[i].cmd, result);
		strcat(checks[i].cmd, " > ");
		strcat(checks[i].cmd, checks[i].file);
		strcat(checks[i].cmd, ".diff");
		system(checks[i].cmd);
		strcpy(result, checks[i].file);
		strcat(result, ".diff");
		fd = open(result, O_RDONLY);
		nb = read(fd, cmp, 10);
		printf("Big %d: %s\n", i, (nb == 0) ? "SUCCESS" : "FAILURE");
		close(fd);
		free(result);
		i++;
	}
}

void	bonus_checks(void)
{
	int		i;
	int		j;
	int		fds[4];
	int		fd;
	char	*line;
	char	*result;

	result = "fichier 1 ligne 1\nfichier 2 ligne 1\nfichier 3 ligne 1\nfichier 1 ligne 2\nfichier 2 ligne 2\nfichier 3 ligne 2\nfichier 1 ligne 3\nfichier 2 ligne 3\nfichier 3 ligne 3\nfichier 1 ligne 4\nfichier 2 ligne 4\nfichier 3 ligne 4\nfichier 1 ligne 5\nfichier 2 ligne 5\nfichier 3 ligne 5\nfichier 1 ligne 6\nfichier 2 ligne 6\nfichier 3 ligne 6\n";
	system("echo \"fichier 1 ligne 1\nfichier 1 ligne 2\nfichier 1 ligne 3\nfichier 1 ligne 4\nfichier 1 ligne 5\nfichier 1 ligne 6\" > sandbox/bonus_1");
	fds[0] = open("sandbox/bonus_1", O_RDONLY);
	system("echo \"fichier 2 ligne 1\nfichier 2 ligne 2\nfichier 2 ligne 3\nfichier 2 ligne 4\nfichier 2 ligne 5\nfichier 2 ligne 6\" > sandbox/bonus_2");
	fds[1] = open("sandbox/bonus_2", O_RDONLY);
	system("echo \"fichier 3 ligne 1\nfichier 3 ligne 2\nfichier 3 ligne 3\nfichier 3 ligne 4\nfichier 3 ligne 5\nfichier 3 ligne 6\" > sandbox/bonus_3");
	fds[2] = open("sandbox/bonus_3", O_RDONLY);
	fds[3] = -1;
	fd = open("sandbox/bonus_result", O_CREAT | O_RDWR | O_TRUNC, 0755);
	i = 0;
	while (i < 6)
	{
		j = 0;
		while (fds[j] >= 0)
		{
			if (get_next_line(fds[j], &line) == 1)
			{
				write(fd, line, strlen(line));
				write(fd, "\n", 1);
			}
			j++;
		}
		i++;
	}
	close(fd);
	fd = open("sandbox/bonus_result", O_RDONLY);
	read(fd, line, 10000);
	close(fd);
	i = 0;
	while (i < 3)
		close(fds[i++]);
	printf("Bonus: %s\n", (strcmp(line, result) == 0) ? "SUCCESS" : "FAILURE");
}

int		main(int argc, char **argv)
{
	int		std_in, simple, big, bonus;

	if (argc != 2)
		return (1);
	simple = big = bonus = std_in = 0;
	if (strcmp(argv[1], "all") == 0)
		simple = big = bonus = std_in = 1;
	else
	{	
		std_in = (strcmp(argv[1], "stdin") == 0) ? 1 : 0;
		simple = (strcmp(argv[1], "simple") == 0) ? 1 : 0;
		big = (strcmp(argv[1], "big") == 0) ? 1 : 0;
		bonus = (strcmp(argv[1], "bonus") == 0) ? 1 : 0;
	}
	system("rm -rf sandbox");
	system("mkdir sandbox");
	if (std_in)
		std_in_checks();
	if (simple)
		simple_checks();
	if (big)
		big_checks();
	if (bonus)
		bonus_checks();
	system("rm -rf sandbox");
	return (0);
}
