if [ "$#" -eq 0 ]; then
	gcc -Wall -Werror -Wextra -c get_next_line.c
	gcc -Wall -Werror -Wextra -c main.c
	gcc -o test_gnl get_next_line.o main.o
	rm *.o
elif [ "$1" == "-l" ]; then
	make -C libft/ fclean
	make -C libft/
	gcc -Wall -Werror -Wextra -I libft/includes/ -c get_next_line.c
	gcc -Wall -Werror -Wextra -I libft/includes/ -c main.c
	gcc -o test_gnl get_next_line.o main.o -L libft/ -lft
	rm *.o
else
	echo "Usage: ./$(basename "$0") [-l]"
fi

