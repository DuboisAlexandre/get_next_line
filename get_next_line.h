/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/07 14:09:59 by adubois           #+#    #+#             */
/*   Updated: 2015/12/11 02:43:33 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 1024
# include <stdlib.h>
# include <unistd.h>
# include <libft.h>

/*
** Structure to store data of the file descriptors. Each file descriptor
** has its one node in the linked list.
** -- Legend --
** fd		file descriptor of the file
** start	index of the next character to read in the buffer
** status	status of the reading (1 = keep going, 0 = EOF found)
** buffer	buffer to store the result of read() on the file descriptor
** next		pointer to the next node
*/

typedef struct	s_gnl_file
{
	int					fd;
	int					start;
	char				buffer[BUFF_SIZE + 1];
	struct s_gnl_file	*next;
}				t_gnl_file;

int				get_next_line(int const fd, char **line);

#endif
