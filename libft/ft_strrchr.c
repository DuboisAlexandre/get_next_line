/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:16:27 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:20:39 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer to the first occurence of the character 'c' in the
** string 's', or NULL if there's no occurences.
*/

char	*ft_strrchr(const char *s, int c)
{
	size_t	i;
	size_t	str_len;
	char	*ptr;

	ptr = NULL;
	str_len = ft_strlen(s);
	i = 0;
	while (i <= str_len)
	{
		if (s[i] == (char)c)
			ptr = (char*)s + i;
		i++;
	}
	return (ptr);
}
