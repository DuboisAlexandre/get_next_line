/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 19:03:03 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:50:05 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Creates a copy of the character string 's1' and returns the copy.
*/

char	*ft_strdup(const char *s1)
{
	char	*dest;

	if ((dest = ft_strnew(ft_strlen(s1))) == NULL)
		return (NULL);
	return (ft_strcpy(dest, s1));
}
