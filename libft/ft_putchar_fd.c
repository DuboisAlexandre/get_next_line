/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 21:15:48 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:26:28 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <libft.h>

/*
** Prints the character 'c' in the output pointed by the file desciptor 'fd'.
*/

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}
