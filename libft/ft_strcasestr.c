/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasestr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:19:49 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:41:47 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns a pointer on the first occurence of the character string 's2' in
** the string 's1'. This function is case insensitive.
*/

char	*ft_strcasestr(const char *s1, const char *s2)
{
	int		i;
	int		j;

	if (s1 == NULL)
		return (NULL);
	if (s2 == NULL || ft_strlen(s2) == 0)
		return ((char*)s1);
	i = 0;
	while (s1[i])
	{
		if (ft_tolower(s1[i]) == ft_tolower(s2[0]))
		{
			j = 0;
			while (s1[i + j] && ft_tolower(s1[i + j]) == ft_tolower(s2[j]))
			{
				if (!s2[j + 1])
					return ((char*)s1 + i);
				j++;
			}
		}
		i++;
	}
	return (NULL);
}
