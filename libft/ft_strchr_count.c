/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_count.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:04:41 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:44:48 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the number of occurences of the character 'c' in the string 's'.
** This function is case sensitive.
*/

int		ft_strchr_count(char *str, int c)
{
	int		count;

	if (str == NULL || c == '\0')
		return (-1);
	count = 0;
	while ((str = ft_strchr(str, c)) != NULL)
	{
		count++;
		str++;
	}
	return (count);
}
