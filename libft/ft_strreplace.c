/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strreplace.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 15:25:34 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:21:19 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Replaces all the occurences of the character string 'needle' in 'haystack'
** by 'replace'.
*/

char	*ft_strreplace(char *haystack, char *needle, char *replace)
{
	return (ft_strnreplace(haystack, needle, replace,
			ft_strstr_count(haystack, needle)));
}
