/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strarraysort.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 18:31:01 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:38:27 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Sort an array of character strings using the function received as parameter.
** The function must return 0 if the two strings are equals, a negative number
** if the first one is lower than the second one and 0 otherwise.
*/

char	**ft_strarraysort(char **tab, int (*cmp)(const char *, const char *))
{
	int		i;
	char	*swap;

	if (tab == NULL)
		return (NULL);
	i = 0;
	while (tab[i + 1] != NULL)
	{
		if ((*cmp)(tab[i], tab[i + 1]) > 0)
		{
			swap = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = swap;
			i = 0;
		}
		else
			i++;
	}
	return (tab);
}
