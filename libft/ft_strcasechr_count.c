/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasechr_count.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:57:50 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:41:03 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the numbe of occurences of the character 'c' in the string 's'.
** This function is case insensitive.
*/

int		ft_strcasechr_count(char *str, int c)
{
	int		count;

	if (str == NULL || c == '\0')
		return (-1);
	count = 0;
	while ((str = ft_strcasechr(str, c)) != NULL)
	{
		count++;
		str++;
	}
	return (count);
}
