/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnodefree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 19:38:12 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:38:58 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Frees the memory used by the content 'content' of the linked list node.
*/

void	ft_lstnodefree(void *content, size_t size)
{
	(void)size;
	ft_memdel(&content);
}
