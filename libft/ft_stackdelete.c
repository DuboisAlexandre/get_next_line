/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stackdelete.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 11:16:00 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:35:44 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Delete the stack pointed by 'stack_ptr' an set the original pointer to NULL.
*/

void	ft_stackdelete(t_stack **stack_ptr)
{
	t_list	*top;

	if (stack_ptr != NULL && *stack_ptr != NULL)
	{
		top = (*stack_ptr)->top;
		ft_lstdel(&top, ft_lstnodefree);
		ft_memdel((void**)stack_ptr);
	}
}
