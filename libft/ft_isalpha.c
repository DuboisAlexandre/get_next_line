/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:48:00 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:35:08 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns '1' if the character 'c' is a lower case letter or an uppercase
** letter, and 0 otherwise.
*/

int		ft_isalpha(int c)
{
	return (ft_isupper(c) || ft_islower(c));
}
