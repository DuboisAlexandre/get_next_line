/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:44:55 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 18:37:32 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Writes 'n' zeroes in memory starting byte pointed by 's'.
*/

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
