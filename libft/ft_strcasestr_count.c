/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasestr_count.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:37:18 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:42:18 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns number of occurences of the character string 's2' in the string
** 's1'. This function is case insensitive.
*/

int		ft_strcasestr_count(char *str, const char *needle)
{
	int		count;

	if (str == NULL || needle == NULL)
		return (-1);
	count = 0;
	while (needle[0] && (str = ft_strcasestr(str, needle)) != NULL)
	{
		count++;
		str++;
	}
	return (count);
}
