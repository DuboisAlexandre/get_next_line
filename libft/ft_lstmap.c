/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 17:35:53 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:02:45 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Iterates overs the nodes of the linked list 'lst', applying the function
** received as parameter to each node and creating a new linked list with
** the nodes returned by the pointed function.
*/

static void	ft_lstmap_free(void *elem, size_t size)
{
	if (elem == NULL)
		return ;
	(void)size;
	ft_memdel(((t_list*)elem)->content);
}

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_list;
	t_list	*new_elem;
	t_list	*old_elem;

	if (f == NULL)
		return (NULL);
	new_list = NULL;
	while (lst != NULL)
	{
		if ((new_elem = (*f)(lst)) == NULL)
		{
			ft_lstdel(&new_list, ft_lstmap_free);
			break ;
		}
		if (new_list == NULL)
			new_list = new_elem;
		else
			old_elem->next = new_elem;
		old_elem = new_elem;
		lst = lst->next;
	}
	return (new_list);
}
