/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplittolst.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 19:14:36 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:26:41 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Splits the character string 's' using the character 'c'. Each substring is
** inserted in a linked list node and the whole list is returned. There's no
** empty node in the linked list.
*/

static void	*ft_strsplittolst_get_line(char const *s, char c, t_list **elem)
{
	static size_t	i = 0;
	size_t			j;
	size_t			line_size;
	char			*line;
	char			*next_c;

	while (s[i] == c)
		i++;
	next_c = ft_strchr(s + i, c);
	line_size = ((next_c) != NULL) ? next_c - (s + i) + 1 : ft_strlen(s + i);
	if ((line = ft_strnew(sizeof(char*) * line_size)) != NULL)
	{
		j = 0;
		while (s[i] && s[i] != c)
			line[j++] = s[i++];
		if (!line[0])
			i = 0;
	}
	if ((*elem = (t_list*)ft_memalloc(sizeof(t_list))) == NULL)
		return (NULL);
	(*elem)->content = (void*)line;
	(*elem)->content_size = line_size;
	(*elem)->next = NULL;
	return ((void*)elem);
}

t_list		*ft_strsplittolst(char const *s, char c)
{
	t_list	*lst;
	t_list	*next_elem;
	t_list	*prev_elem;

	lst = NULL;
	while (s)
	{
		if ((ft_strsplittolst_get_line(s, c, &next_elem) == NULL)
			|| next_elem->content == NULL)
		{
			ft_lstdel(&lst, ft_lstnodefree);
			break ;
		}
		if (ft_strlen((char*)next_elem->content) == 0)
		{
			ft_lstdelone(&next_elem, ft_lstnodefree);
			break ;
		}
		if (lst == NULL)
			lst = next_elem;
		else
			prev_elem->next = next_elem;
		prev_elem = next_elem;
	}
	return (lst);
}
