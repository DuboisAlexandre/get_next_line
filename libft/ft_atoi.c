/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:28:18 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:34:21 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Converts the ASCII string 'str' to an interger.
*/

int		ft_atoi(const char *str)
{
	int		i;
	int		n;
	int		negative;

	i = 0;
	while (ft_isspace(str[i]))
		i++;
	negative = (str[i] == '-') ? 1 : 0;
	if (str[i] == '-' || str[i] == '+')
		i++;
	n = 0;
	while (ft_isdigit(str[i]))
		n = n * 10 + (str[i++] - '0');
	if (negative)
		n = -n;
	return (n);
}
