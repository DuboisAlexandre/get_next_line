/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr_count.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 10:25:37 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:28:07 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns number of occurences of the character string 's2' in 's1'.
*/

int		ft_strstr_count(char *str, const char *needle)
{
	int		count;

	if (str == NULL || needle == NULL)
		return (-1);
	count = 0;
	while (needle[0] && (str = ft_strstr(str, needle)) != NULL)
	{
		count++;
		str++;
	}
	return (count);
}
