/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stackpop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 10:31:19 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:36:14 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the next node in the stack 'stack'.
*/

t_list	*ft_stackpop(t_stack *stack)
{
	t_list	*node;

	if (stack == NULL)
		return (NULL);
	node = stack->top;
	if (stack->top != NULL)
	{
		stack->top = stack->top->next;
		stack->size -= 1;
	}
	return (node);
}
