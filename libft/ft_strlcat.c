/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 13:38:07 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 20:01:34 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Concatenates the character string 'src' to 'dst' and guarantee to NULL
** terminate the result without overflowing 'dst'. If 'dst' has no NULL
** terminator in the first 'size' characters, the result will not be NULL
** terminated. The function returns the original size of 'dst' plus the
** size of 'src'. This make the the check of a truncation easier.
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_len;
	size_t	src_len;
	int		limit;
	int		i;

	src_len = ft_strlen(src);
	dst_len = ft_strnlen(dst, size);
	limit = size - dst_len - 1;
	i = 0;
	while (src[i] && i < limit)
	{
		dst[dst_len + i] = src[i];
		i++;
	}
	if (dst_len < size && dst_len + i <= sizeof(dst))
		dst[dst_len + i] = '\0';
	return (dst_len + src_len);
}
