/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstprintstr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 15:28:09 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:08:28 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Prints the content of a linked list node received as parameter as if it
** was character string.
*/

void	ft_lstprintstr(t_list *list)
{
	while (list != NULL)
	{
		if (list->content == NULL)
			ft_putendl("(null)");
		else
			ft_putendl((char*)list->content);
		list = list->next;
	}
}
