/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 20:02:44 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:45:53 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Sets all the bytes of character string 's' before the terminal '\0' to 0.
*/

void	ft_strclr(char *s)
{
	if (s != NULL)
		ft_bzero((void*)s, ft_strlen(s));
}
