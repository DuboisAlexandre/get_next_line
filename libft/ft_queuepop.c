/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queuepop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 13:53:24 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:33:40 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Returns the next node of the queue 'queue'.
*/

t_list	*ft_queuepop(t_queue *queue)
{
	t_list	*node;

	if (queue == NULL)
		return (NULL);
	node = queue->top;
	if (queue->top != NULL)
	{
		queue->top = queue->top->next;
		queue->size -= 1;
	}
	return (node);
}
