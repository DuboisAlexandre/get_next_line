/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 16:08:27 by adubois           #+#    #+#             */
/*   Updated: 2015/11/29 19:11:42 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** Sort the linked list pointed by 'list' using the function received as
** parameter. The pointed function must return 0 if the nodes are equals,
** a negative number if the first node is lower than the second and a
** positive number otherwise.
*/

void	ft_lstsort(t_list **list, int (*cmp)(const t_list *, const t_list *))
{
	t_list	*node;
	t_list	*next;
	t_list	*previous;
	t_list	**parent;

	if (list == NULL || *list == NULL || cmp == NULL)
		return ;
	previous = NULL;
	node = *list;
	while (node->next != NULL)
		if ((*cmp)(node, node->next) > 0)
		{
			next = node->next;
			node->next = next->next;
			next->next = node;
			parent = (previous == NULL) ? list : &previous->next;
			*parent = next;
			node = *list;
			previous = NULL;
		}
		else
		{
			previous = node;
			node = node->next;
		}
}
